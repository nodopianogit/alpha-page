<?php
Namespace App\Controllers;

use Nodopiano\Corda\Controller;
use Nodopiano\Corda\Mail;
use Nodopiano\Corda\NewsLetter;
use Nodopiano\Corda\Csv;

/**
 *
 */
class MailController extends Controller
{

    public function send()
    {
      // Csv::export(array($_POST['from'],$_POST['message']));
      // NewsLetter::subscribe('38e73a2904',array('email' => $_POST['from']));
    	$messaggio = $_POST['name'].'<br/><br/>'.$_POST['message'];
        $mail = Mail::send($_POST['from'],'Email da Alpha Page', $messaggio);

        $data = $this->api->pages(getenv('API_PAGE_ID'));
        if ($mail)  return view('thankyou.html', array('data' => $data, 'long_message' => 'Il tuo messaggio è stato inviato correttamente.<br/>Riceverai una risposta al più presto.', 'message' => 'Grazie!' ));
        else return view('thankyou.html', array('data' => $data ,'long_message' => "Siamo spiacenti.<br/>Si è verificato un errore durante l'invio del messaggio, riprova più tardi.", 'message' => 'Errore!' ));

    }
}
