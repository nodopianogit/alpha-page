<?php
Namespace App\Controllers;

use Nodopiano\Corda\Controller;

class PrivacyController extends Controller
{

  public function privacy()
  {
    $data = $this->api->pages(getenv('API_PAGE_ID'));
    $privacy = $this->api->pages(getenv('PRIVACY_PAGE_ID'));
    return view('policy.html',array('data' => $data, 'page' => $privacy, 'google_api' => 'AIzaSyAVaG_sfpL-tOLnvNwLF04Zt-o2fVaxhfI'));
  }

  public function cookie()
  {
    $data = $this->api->pages(getenv('API_PAGE_ID'));
    $cookie = $this->api->pages(getenv('COOKIE_PAGE_ID'));
    return view('policy.html',array('data' => $data, 'page' => $cookie,'google_api' => 'AIzaSyAVaG_sfpL-tOLnvNwLF04Zt-o2fVaxhfI'));
 
  }
}
