$('.slideshow__inner').slick({
  adaptiveHeight: true,
  arrows: true,
  dots: true,
  fade: true,
  infinite: false,
  slidesToShow: 1,
  speed: 750,
  responsive: [
    {
      breakpoint: 640,
      settings: {
        dots: false
      }
    }
  ]
});